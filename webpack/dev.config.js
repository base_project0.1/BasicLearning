const webpack = require('webpack')
const common = require('./common')

const HtmlPlugin = require('html-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const DashboardPlugin = require('webpack-dashboard/plugin')

module.exports = {
  devtool: 'source-map',

  entry: [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:3002',
    'webpack/hot/only-dev-server',
  ].concat(common.entry),

  output: Object.assign({}, common.output, {
    filename: '[name].js',
  }),

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    // new BundleAnalyzerPlugin(),
    new DashboardPlugin(),

    new HtmlPlugin(common.htmlPluginConfig),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    /*new webpack.DefinePlugin({
      __HOST__:
      //'"http://localhost:51888/api"',
      '"http://cpro36838.publiccloud.com.br:8083/api"',
      TIMEOUT: 5000,
      HEADERS: {
        'Access-Control-Allow-Origin': '"*"',
      },
    }),*/
  ],

  module: {
    rules: [
      common.jsLoader,
      common.cssLoader,
      common.stylusLoader,
      common.fileLoader,
      common.urlLoader,
    ],
  },

  resolve: common.resolve,
}
