import styled from 'styled-components'
import bgBlack from './bg-black2.jpg'

const style = styled.div`
  position: relative;
  background-color: #ecede8;
  border: black 1px solid;
  border-radius: 0 0 10px 10px;
  box-shadow: 0px 3px 0px 0px #0000004a;
  padding: 5px;

  font-family: Lato;

${'' /* Genetal Styles */}
  .title {
    color: #c4ca60;
    text-transform: uppercase;
    font-family: Montserrat;
    font-weight: bolder;
    padding: 40px 0 20px 0;
    letter-spacing: 0.5em;
  }

i {
  color: #c4ca60;
  margin-top: 30px;
}

.toTop {
  position:fixed;
  right: 0;
  top:0;
  margin-top: 0;
  z-index: 999;
  background-color: #c4ca60;
  border-radius: 5px 0 5px 5px;
  border: 1px #00000054 solid;
  padding: 5px;
  cursor:pointer;
}

.toTop > i {
  color: #373c5a;
  margin-top: 0;
}

.toTopLeft {
  position:fixed;
  left: 0;
  top:0;
  margin-top: 0;
  z-index: 999;
  background-color: #c4ca60;
  border-radius: 0px 5px 5px 5px;
  border: 1px #00000054 solid;
  padding: 5px;
  cursor:pointer;
}

.toTopLeft > i {
  color: #373c5a;
  margin-top: 0;
}

${'' /* Header Styles */}
  .header {
    background-color: rgba(0,0,0,0.75);
    margin-bottom: 50px;
    padding: 20px;
    background-image: url(${bgBlack});
    background-size: cover;
    background-blend-mode: color;
    justify-content: center;
    text-align: center;
  }

  .mainName {
    font-size: 50px;
    font-weight:bold;
    display: flex;
    justify-content: center;
    text-align: center;
    letter-spacing: 0.5em;
  }

  .mainTitle {
    color: #ecede8;
    font-size: 20px;
    text-transform: uppercase;
    padding: 50px 0;

    text-align: center;
  }

  .desc.white{
    color: #ecede8;
    text-align: center;
  }

${'' /* Content Styles */}
  .content {
    background-color: white;
    padding: 20px;
  }

  .subTitle {
    color: #373c5a;
    font-size: 17px;
    font-weight: bolder;
    padding: 10px 0px;
  }

  .desc.black{
    color: #373c5a;
  }

  ${'' /* Hability Styles */}
  .habilityGraph {
    height: 10px;
    margin: 2px;
  }

  .bgGraph {
    padding: 2px;
    ${'' /* background-color: black; */}
    width: 100%;
    height: 100%;
    border-radius: 5px;
    border: black 1px solid;
  }

  .bgValue {
    background-color: #9e9e9e;
    height: 100%;
    border-radius: 5px
  }




  .container-fluid, .row, div[class^='col-'] {
    padding: 0px;
    margin: 0px;
    ${'' /* max-width: calc(100% - 5em); */}
  }

  ${'' /* div[class^='col-'][class$='-0'] {
    display: none;
  } */}

`
export default style
