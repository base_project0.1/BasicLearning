import React from 'react'

import { Grid, Row, Col } from 'react-flexbox-grid'
import Style from './style'

import './css/font-awesome.min.css'

// lg – 75em (most often 1200px) and up
//
// md – 64em (most often 1024px) to 74em
//
// sm – 48em (most often 768px) to 63em
//
// xs – up to 47em

class HomePage extends React.Component {

  componentDidMount() {
    this.TryToShowBtnToTop()
    window.addEventListener('scroll', () => this.TryToShowBtnToTop())
    window.addEventListener('resize', () => this.onResize())
  }

  onResize() {
    this.TryToShowBtnToTop()
  }

  TryToShowBtnToTop() {
    this.btnToTop.style.display = window.pageYOffset >= 80 && window.outerWidth < 820 ? "" : "none"
  }

  habilityCreate(title, width, color) {
    return (
      <Row>
        <Col xs={6} sm={6} md={6} lg={6}>
          <div className="desc black">{title}</div>
        </Col>
        <Col xs={6} sm={6} md={6} lg={6}>
          <div className="habilityGraph">
            <div className="bgGraph">
              <div className="bgValue" {...this.styleGraph(width, color)} />
            </div>
          </div>
        </Col>
      </Row>
    )
  }

  styleGraph(w, c) {
    return { style:{ width: w + "%", backgroundColor: c || "#9e9e9e" } }
  }

  backToTop() {
    window.scrollTo(0,0)
  }

  render() {

    const iconSize = "fa-2x"
    const headerName = { xs: 12, sm: 6, md: 12, lg: 12 }

    return (
      <Style>
        <Grid fluid>
          <Row>
            <Col xs={2} sm={2} md={0} lg={0} >
              <div className=" toTop" onClick={this.backToTop} ref={(r) => { this.btnToTop = r }}>
                <i className={"fa fa-angle-double-up ".concat(iconSize)} title="Topo" aria-hidden="true" />
              </div>
            </Col>
            <Col xs={2} sm={2} md={2} lg={2} >
              <a href="media/MARCELA_HELENA_ROSIM_-_CV.pdf" target="_blank" className=" toTopLeft">
                <i className={"fa fa-download ".concat(iconSize)} title="CV" aria-hidden="true" />
              </a>
            </Col>
          </Row>
        </Grid>
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={5} lg={5}>
              <div className="header">
                <Row>
                  <Col {...headerName}>
                    <div className="mainName title"><strong>Marcela</strong></div>
                  </Col>
                  <Col {...headerName}>
                    <div className="mainName title"><strong>Rosim</strong></div>
                  </Col>
                </Row>
                <div className="mainTitle">Assistente de Engenharia</div>
                <i className={"fa fa-map-marker ".concat(iconSize)} title="Localização" aria-hidden="true" />
                <div className="desc white">Vila Andrade - São Paulo/SP - Brasil</div>

                <i className={"fa fa-phone ".concat(iconSize)} title="Celular" aria-hidden="true" />
                <div className="desc white">(11) 99440 7777</div>

                <i className={"fa fa-envelope ".concat(iconSize)} title="E-mail" aria-hidden="true" />
                <div className="desc white">mhrosim@gmail.com</div>
              </div>
            </Col>
            <Col xs={12} sm={12} md={7} lg={7}>
              <div className="content">
                {/* Acadêmico */}
                <div className="title">Acadêmico</div>
                <div className="subTitle">Estudante de Engenharia Civil (Noturno)</div>
                <div className="desc black">Universidade Anhembi Morumbi | 2014 - 2019</div>

                {/* Experiências */}
                <div className="title">Experiências</div>
                <div className="subTitle">MSC Cruise (Malta)</div>
                <div className="desc black">Grande Porte - Cruseiros | Jan 2018 - Ago 2018</div>
                <li className="desc black">Responsável pelo atendimento geral ao cliente no bares do navio.</li>
                <li className="desc black">Resultados obtidos: desenvolvimento cultural, humano e social, aprendizado e prática de línguas estrangeiras.</li>

                <div className="subTitle">Soloconsult</div>
                <div className="desc black">Pequeno Porte | Abr 2013 - Jan 2018</div>
                <li className="desc black">Responsável pelo criação de desenhos de projetos, detalhes, memorial descritivo, memória de cálculo, relatório técnico e fotográfico, orçamento, acompanhamento em visitas técnicas.</li>
                <li className="desc black">Resultados obtidos: desenvolvimento de análise crítica, padronizada e perfeccionista.</li>

                {/* Cursos */}
                <div className="title">Cursos</div>
                <div className="subTitle">Engenharia e Gestão de Obras</div>
                <div className="desc black">FAAP - 2016</div>

                <div className="subTitle">Segurança e Saúde em Trabalhos na Altura</div>
                <div className="desc black">Qteck - 2015</div>

                <div className="subTitle">AutoCAD Avançado</div>
                <div className="desc black">Impacta - 2012</div>

                <div className="subTitle">AutoCAD Básico</div>
                <div className="desc black">Senac - 2009</div>

                {/* Habilidades */}
                <div className="habilities">
                  <div className="title">Habilidades</div>
                  <Grid fluid>
                    {this.habilityCreate("AutoCAD", 90)}
                    {this.habilityCreate("Office", 80)}
                    {this.habilityCreate("Inglês", 75)}
                    {this.habilityCreate("Photoshop", 65)}
                    {this.habilityCreate("MS Project", 60)}
                    {this.habilityCreate("Relações Interpessoais", 80)}
                    {this.habilityCreate("Cominicação", 80)}
                    {this.habilityCreate("Trabalho em Equipe", 80)}
                  </Grid>
                </div>

                <div className="title">Interesses</div>
                <div className="desc black">Investimentos financeiros</div>
                <div className="desc black">Empreendedorismo</div>
                <div className="desc black">Gestão e Coordenação</div>
                <div className="desc black">Viagens</div>
                <div className="desc black">Livros e Filmes</div>
                <div className="desc black">Novas culturas</div>
              </div>
            </Col>
          </Row>
        </Grid>
      </Style>
    )
  }
}

export default HomePage
