import HomePage from './HomePage'


import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import { BrowserRouter, Route, Link } from "react-router-dom"

function renderApp() {
  ReactDOM.render(
    <AppContainer>
      <div>
        <BrowserRouter>
          <div>
            <Route exact path="/" component={HomePage} />
          </div>
        </BrowserRouter>
      </div>
    </AppContainer>,
    document.querySelector('[data-js="app"]')
  )
}

let isFirst = false
if (module.hot || isFirst) {//apenas em modo de desenvolvimento
  renderApp()
  isFirst = false
}
