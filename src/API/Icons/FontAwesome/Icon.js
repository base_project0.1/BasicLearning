import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import 'font-awesome/css/font-awesome.min.css'

const StyledIcon = styled.i`
  &.fa {
    font-size: ${props => props.size}px;
  }
`
const Icon = ({ icon, className, ...rest }) => (
  <StyledIcon className={`fa fa-${icon} ${className}`} {...rest} />
)

Icon.defaultProps = {
  size: 14,
}

Icon.propTypes = {
  icon: PropTypes.string.isRequired,
  className: PropTypes.string,
}

export default Icon
